using Microsoft.VisualStudio.TestTools.UnitTesting;
using API;

namespace TestMath
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MathMethod1()
        {
    //Arrange : Inicializar las variables
     decimal value1 = 2;
     decimal value2 = 3;

    //Act : llamar al metodo a testear
     decimal resultado = Math.Add(value1, value2);

    //Assert: comprobar el valor con el esperado.
      Assert.AreEqual(5, resultado);

        }
        [TestMethod]
        public void MathMethod2()
        {
    //Arrange : Inicializar las variables
     decimal value1 = 5;
     decimal value2 = 3;

    //Act : llamar al metodo a testear
     decimal resultado = Math.Substract(value1, value2);

    //Assert: comprobar el valor con el esperado.
      Assert.AreEqual(2, resultado);

        }
        [TestMethod]
        public void MathMethod3()
        {
    //Arrange : Inicializar las variables
     decimal value1 = 5;
     decimal value2 = 3;

    //Act : llamar al metodo a testear
     decimal resultado = Math.Multiply(value1, value2);

    //Assert: comprobar el valor con el esperado.
      Assert.AreEqual(15, resultado);

        }
        [TestMethod]
        public void MathMethod4()
        {
    //Arrange : Inicializar las variables
     decimal value1 = 4;
     decimal value2 = 2;

    //Act : llamar al metodo a testear
     decimal resultado = Math.Divide(value1, value2);

    //Assert: comprobar el valor con el esperado.
      Assert.AreEqual(2, resultado);

        }
    }
}